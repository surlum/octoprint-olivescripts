/*
 * View model for Olive Scripts
 *
 * Author: Surlum
 * License: AGPLv3
 * https://docs.octoprint.org/en/master/plugins/viewmodels.html
 */
$(function() {
    function OliveScriptsViewModel(parameters) {
        var self = this;
        // assign the injected parameters, e.g.:
        self.settings = parameters[0];
        self.loginState = parameters[1];
        self.connectionState = parameters[2];
        self.terminal = parameters[3];

        self.onDataUpdaterPluginMessage = function(plugin, data) {
            if(plugin != "olivescripts") return;
            if(data.msg == "") return;

            new PNotify({
                title: 'Olive Scripts Plugin',
                text: data.msg,
                type: data.type,
                hide: true
            });
		};

		self.executeMacro = function() {
		    if(!self.isActive()) return;

            self.terminal.command("@OLIVE DEV");
            self.terminal.sendCommand();
		};

		self.isActive = function (data) {
		    //const dop = data.dop();
		    //if (dop && self.connectionState.isPrinting()) {
		    //    return false;
		    //}
		    return self.connectionState.isOperational() && self.loginState.isUser();
		};

        self.get_color = function (data) {
            if(self.isActive()) {
                return "green";
            }

            return "gray";
        };
    }

    /* view model class, parameters for constructor, container to bind to
     * Please see http://docs.octoprint.org/en/master/plugins/viewmodels.html#registering-custom-viewmodels for more details
     * and a full list of the available options.
     */
    OCTOPRINT_VIEWMODELS.push({
        construct: OliveScriptsViewModel,
        // ViewModels your plugin depends on, e.g. loginStateViewModel, settingsViewModel, ...
        dependencies: ["settingsViewModel", "loginStateViewModel", "connectionViewModel", "terminalViewModel"],
        // Elements to bind to, e.g. #settings_plugin_olivescripts, #tab_plugin_olivescripts, ...
        elements: [ "#navbar_plugin_olivescripts" ]
    });
});
