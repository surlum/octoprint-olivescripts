import hashlib
import os
import shutil

def generate_hash(file: str):
    """
    Generate sha256 hash for a given file

    :param file: Input file
    :return: haxadecimal digits
    :rtype: str
    """

    # Influenced by:
    # https://www.programiz.com/python-programming/examples/hash-file
    # https://www.pythoncentral.io/hashing-files-with-python/
    # https://nitratine.net/blog/post/how-to-hash-files-in-python/

    if not os.path.isfile(file):  # Check if the file exists
        return None  # It does not

    block_size = 65536  # The size of each read from the file
    f_hash = hashlib.sha256()  # Create the hash object

    with open(file, 'rb') as f:  # Open the file to read its bytes
        fb = f.read(block_size)  # Read the first block from the file
        while len(fb) > 0:  # While there is still data to read
            f_hash.update(fb)  # Update the hash
            fb = f.read(block_size)  # Read the next block from the file

    return f_hash.hexdigest()  # Return the hexadecimal digest of the hash

def is_custom_gcode_script(file: str):
    """
    Checks first line of file for '; CUSTOM'

    :param file: File to check
    :return: True if found
    :rtype: bool
    """
    if not os.path.isfile(file):  # Check if the file exists
        return False

    with open(file, 'rt') as f:  # Open the file for reading as text mode (default)
        line = f.readline()
        line = line.upper()
        if line.startswith("; CUSTOM") or line.startswith(";CUSTOM"):
            return True
    return False

def replace_script(destination_file: str, source_file: str):
    """
    Overwrites a destination file with the contents of a source file

    :param destination_file: File to overwrite
    :param source_file: File with new data
    :return: None
    """

    # Make sure the source file exists
    if not os.path.isfile(source_file):
        return

    # Get the source file contents
    with open(source_file, 'rt') as f:
        if f.readable():
            source_data = f.read()  # get contents
        else:
            source_data = None

    # Check contents
    if source_data is None:
        return

    # Make sure the destination path exists
    d_path, d_file = os.path.split(destination_file)
    if not os.path.exists(d_path):
        return

    # Open the destination file 'w' to overwrite
    with open(destination_file, 'w') as f:
        if f.writable():
            f.write(source_data)  # write contents

    return

def archive_script_file(file_name: str, current_folder: str, destination_folder: str):
    """
    Moves a file from one folder to another

    :param file_name: foo.bar
    :param current_folder: /path/to/current/
    :param destination_folder: /path/to/destination/
    :return: None
    """

    # Define the /path/to/current/foo.bar and /path/to/destination/foo.bar
    current_file_and_path = os.path.realpath(os.path.join(current_folder, file_name))
    destination_file_and_path = os.path.realpath(os.path.join(destination_folder, file_name))

    # Make sure the current file exists
    if not os.path.isfile(current_file_and_path):
        return

    # Check for the destination folder
    if not os.path.exists(destination_folder):
        # Make one if it doesn't exist
        os.makedirs(destination_folder)

    # Move the file
    shutil.move(current_file_and_path, destination_file_and_path)
    return
