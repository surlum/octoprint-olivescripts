# @COMMAND
AT_COMMAND = "OLIVE"

# @COMMAND PARAMETERS
AT_PARAM_COOL_MACHINE = "COOL"
AT_PARAM_CURA_START = "CURA_START"
AT_PARAM_CURA_END = "CURA_END"
AT_PARAM_HOME = "HOME"
AT_PARAM_OCTO_START = "OCTO_START"
AT_PARAM_OCTO_END = "OCTO_END"
AT_PARAM_PREHEAT_1 = "PREHEAT1"
AT_PARAM_PREHEAT_2 = "PREHEAT2"
AT_PARAM_PRESENT_PRINT = "PRESENT"
AT_PARAM_PURGE_NOZZLE = "PURGE_NOZZLE"
AT_PARAM_RETRACT_AND_WIPE = "RETRACT_AND_WIPE"
AT_PARAM_SETUP = "SETUP"
AT_PARAM_DEVELOPER = "DEV"

# SOFTWARE UPDATE
GITLAB_PROJECT_ID = 37527922
GET_RELEASES_URL = "https://gitlab.com/api/v4/projects/%s/releases" % GITLAB_PROJECT_ID
RELEASE_ZIP_URL = "https://gitlab.com/surlum/octoprint-olivescripts/-/archive/{target}/octoprint-olivescripts-{target}.zip"
RELEASE_NOTES_URL = "https://gitlab.com/surlum/octoprint-olivescripts/-/releases/%s"
