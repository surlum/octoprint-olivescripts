# coding=utf-8
from __future__ import absolute_import
import os

import octoprint.plugin
from .version_checker import OliveScriptsPluginVersionChecker
from . import constants
from .util import generate_hash, is_custom_gcode_script, replace_script, archive_script_file

# Sources
# https://docs.octoprint.org/en/master/plugins/gettingstarted.html

class OliveScriptsPlugin(octoprint.plugin.StartupPlugin,
                         octoprint.plugin.SettingsPlugin,
                         octoprint.plugin.AssetPlugin,
                         octoprint.plugin.TemplatePlugin
                         ):

    ##~~ StartupPlugin mixin

    def on_after_startup(self):
        self.update_plugin_scripts()  # Note: Software handles updated files without intervention
        return

    # def on_startup(self): return

    ##~~ SettingsPlugin mixin

    # TODO: Implement settings
    #   1. default script settings
    #   2. (+) add macro
    #   3. @macro = scriptFile [custom] [edit] [remove]
    #   4. popup_textbox with script lines
    # def get_settings_defaults(self):
    #     return {
    #     }

    ##~~ AssetPlugin mixin

    @staticmethod
    def get_assets():
        return {
            "js": ["js/olivescripts.js"],
            "css": ["css/olivescripts.css"],
            "less": ["less/olivescripts.less"]
        }

    ##~~ SoftwareUpdate hook

    def get_update_information(self):
        # See https://docs.octoprint.org/en/master/bundledplugins/softwareupdate.html
        return {
            "olivescripts": {
                "displayName": self._plugin_name,
                "displayVersion": self._plugin_version,
                "current": self._plugin_version,
                "type": "python_checker",
                "python_checker": OliveScriptsPluginVersionChecker(),
                "pip": constants.RELEASE_ZIP_URL
            }
        }

    # @ command handling
    def process_at_command(
        self, comm, phase, command: str, parameters: str, tags=None,
        *args, **kwargs
    ):
        # Format command and parameters for processing
        command = command.upper()
        parameters = parameters.upper()

        # Handle old command hooks
        if command == "OCTO_START_HOOK":
            command = constants.AT_COMMAND
            parameters = constants.AT_PARAM_OCTO_START
        if command == "OCTO_END_HOOK":
            command = constants.AT_COMMAND
            parameters = constants.AT_PARAM_OCTO_END
        if command == "CURA_START_HOOK":
            command = constants.AT_COMMAND
            parameters = constants.AT_PARAM_CURA_START
        if command == "CURA_END_HOOK":
            command = constants.AT_COMMAND
            parameters = constants.AT_PARAM_CURA_END

        # Check if this is a supported command
        if command != constants.AT_COMMAND:
            return

        job = self._printer.get_current_job()
        file = job['file']
        filename = file['name']
        #        self._logger.info("Job File: {}".format(filename))

        block_flag = False  # Is Continuous Print Plugin Script
        if filename is not None:
            if filename.startswith("cp_") and filename.endswith("_script.gcode"):
                # https://github.com/smartin015/continuousprint
                # Reference: /continuousprint/data/__init__.py
                block_flag = True  # Matches Continuous Print

        if parameters == constants.AT_PARAM_COOL_MACHINE:
            self.runScript("oliveCoolMachine", block_flag)
            return
        if parameters == constants.AT_PARAM_CURA_START:
            self.runScript("oliveCuraStart", block_flag)
            return
        if parameters == constants.AT_PARAM_CURA_END:
            self.runScript("oliveCuraEnd", block_flag)
            return
        if parameters == constants.AT_PARAM_HOME:
            self.runScript("oliveHome", block_flag)
            return
        if parameters == constants.AT_PARAM_OCTO_START:
            self.runScript("oliveOctoStart", block_flag)
            return
        if parameters == constants.AT_PARAM_OCTO_END:
            self.runScript("oliveOctoEnd", block_flag)
            return
        if parameters == constants.AT_PARAM_PREHEAT_1:
            self.runScript("olivePreheat1", block_flag)
            return
        if parameters == constants.AT_PARAM_PREHEAT_2:
            self.runScript("olivePreheat2", block_flag)
            return
        if parameters == constants.AT_PARAM_PRESENT_PRINT:
            self.runScript("olivePresentPrint", block_flag)
            return
        if parameters == constants.AT_PARAM_PURGE_NOZZLE:
            self.runScript("olivePurgeNozzle", block_flag)
            return
        if parameters == constants.AT_PARAM_RETRACT_AND_WIPE:
            self.runScript("oliveRetractAndWipe", block_flag)
            return
        if parameters == constants.AT_PARAM_SETUP:
            self.runScript("oliveSetupMachine", block_flag)
            return
        if parameters == constants.AT_PARAM_DEVELOPER:
            self.developer_test()
            return

    def runScript(self, script_name: str, blocked: bool = False):
        if blocked:
            return  # Don't execute the script
        self._printer.script(script_name)
        # TODO: clear commented code after testing
        #  self.popup_message(dict(type="info", msg="Called Script:<BR>{}".format(script_name)))
        self.popup_message("Called Script:<BR>{}".format(script_name))
        self._logger.info("Called script {}".format(script_name))

    def popup_message(self, message_text, message_type: str = "info"):
        # https://github.com/smartin015/continuousprint/blob/master/continuousprint/__init__.py
        # https://github.com/jneilliii/OctoPrint-M117PopUp/blob/master/octoprint_M117PopUp/__init__.py
        # https://plugins.octoprint.org/plugins/M117PopUp/
        # https://docs.octoprint.org/en/master/plugins/viewmodels.html
        # See olivescripts_viewmodel.js onDataUpdaterPluginMessage
        # TODO: clear commented code after testing
        #  self._plugin_manager.send_plugin_message(self._identifier, data)
        self._plugin_manager.send_plugin_message(self._identifier, dict(type=message_type, msg=message_text))

    def developer_test(self):
        # self.update_plugin_scripts()
        self.popup_message("Button Clicked")
        return

    def update_plugin_scripts(self):
        """
        Compares files from .octoprint/scripts/gcode and <plugin>/static/gcode and updates as needed.
        Ignores files that start with ;custom

        :return: None
        """

        # Influenced by:
        # https://docs.octoprint.org/en/master/modules/settings.html#octoprint.settings.Settings.saveScript

        self._logger.info("Checking plugin files for updates")

        # Get update and archive folders for for plugin
        #   (self._basefolder) returns: .../venv/lib/python#.##/site-packages/octoprint_<plugin>
        update_folder = os.path.realpath(os.path.join(self._basefolder, "static", "gcode"))  # <plugin>/static/gcode
        archive_folder = os.path.realpath(os.path.join(self._basefolder, "static", ".gcode"))  # <plugin>/static/.gcode

        # Check to make sure the update folder exists
        if not os.path.exists(update_folder):
            self._logger.warning("Folder not found: {}".format(update_folder))
            return  # It does not, stop update

        # Get Octoprint script/gcode folder
        octo_folder = self._settings.getBaseFolder("scripts")  # ~/.octoprint/scripts
        octo_folder = os.path.realpath(os.path.join(octo_folder, "gcode"))  # ~/.octoprint/scripts/gcode

        # Check to make sure the octoprint folder for scripts exists
        if not os.path.exists(octo_folder):
            self._logger.warning("Folder not found: {}".format(octo_folder))
            return  # It does not, stop update

        # Get list of files from the update folder
        update_scripts = os.listdir(update_folder)  # [script1, script2, ...]
        if len(update_scripts) == 0:  # Are there any files to process?
            return  # No files found, stop update

        # Run through the list of files and see if any updates are needed
        for script_name in update_scripts:
            old_file = os.path.realpath(os.path.join(octo_folder, script_name))  # /path/to/old/file.gcode
            custom_flag = is_custom_gcode_script(old_file)  # Check file for custom flag

            if not custom_flag:  # Only process the update if the old file isn't marked custom
                old_file_hash = generate_hash(old_file)  # Calculate hash for old file
                update_file = os.path.realpath(os.path.join(update_folder, script_name))  # /path/to/update/file.gcode
                update_file_hash = generate_hash(update_file)  # Calculate hash for update file

                if update_file_hash is None:
                    #  Uh oh, the update is empty.
                    self._logger.info("Empty update file: {}".format(script_name))
                    archive_script_file(script_name, update_folder, archive_folder)
                    continue

                if old_file_hash is None or old_file_hash != update_file_hash:
                    # Replace current script with new script
                    self._logger.info("Updated file: {}".format(script_name))
                    replace_script(old_file, update_file)
            else:  # File is marked as custom
                self._logger.info("Skipping file: {} (custom)".format(script_name))

            # Move script file to archive
            archive_script_file(script_name, update_folder, archive_folder)

        return

# If you want your plugin to be registered within OctoPrint under a different name than what you defined in setup.py
# ("OctoPrint-PluginSkeleton"), you may define that here. Same goes for the other metadata derived from setup.py that
# can be overwritten via __plugin_xyz__ control properties. See the documentation for that.
__plugin_name__ = "Olive Scripts Plugin"

# Set the Python version your plugin is compatible with below. Recommended is Python 3 only for all new plugins.
# OctoPrint 1.4.0 - 1.7.x run under both Python 3 and the end-of-life Python 2.
# OctoPrint 1.8.0 onwards only supports Python 3.
__plugin_pythoncompat__ = ">=3,<4"  # Only Python 3


def __plugin_load__():
    global __plugin_implementation__
    __plugin_implementation__ = OliveScriptsPlugin()

    global __plugin_hooks__
    __plugin_hooks__ = {
        "octoprint.plugin.softwareupdate.check_config": __plugin_implementation__.get_update_information,
        "octoprint.comm.protocol.atcommand.queuing": __plugin_implementation__.process_at_command
    }
