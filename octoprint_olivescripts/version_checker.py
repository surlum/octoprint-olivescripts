from distutils.version import LooseVersion
import requests

from . import constants

# Resources:
#     https://gitlab.com/mosaic-mfg/canvas-plugin/-/tree/master
#     https://community.octoprint.org/t/gitlab-as-a-plugin-repository/7124/8
#     https://gitlab.com/mosaic-mfg/canvas-plugin/-/tree/master
#     https://docs.octoprint.org/en/master/bundledplugins/softwareupdate.html

class OliveScriptsPluginVersionChecker:
    # Resource:
    def __init__(self):
        pass

    @staticmethod
    def get_latest(target, check, full_data=False, online=True):
        try:
            resp = requests.get(constants.GET_RELEASES_URL)
            version_data = resp.json()
            latest_version_info = version_data[0]
            version = latest_version_info['name']
            current_version = check.get("current")
        except KeyError:
            version = check.get("current")
            current_version = check.get("current")

        information = dict(
            local=dict(
                name=current_version,
                value=current_version,
                release_notes=constants.RELEASE_NOTES_URL % current_version
            ),
            remote=dict(
                name=version,
                value=version,
                release_notes=constants.RELEASE_NOTES_URL % version
            )
        )

        needs_update = LooseVersion(current_version) < LooseVersion(version)

        return information, not needs_update
